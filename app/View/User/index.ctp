<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login PinkBlue</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('app/webroot/img/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="/User/submit">
					<span class="login100-form-logo">
                                            <img src = "app/webroot/img/pinkBlue.png">
						
					</span>
                                    <?php
                                            if (isset($messageType) && strcasecmp($messageType, 'SUCCESS') === 0) {
                                                $messageClass = "alert alert-success";
                                            } else if (isset($messageType) && strcasecmp($messageType, 'FAILURE') === 0) {
                                                $messageClass = "alert alert-danger";
                                            }

                                            if (isset($message) && !empty($message)) {
                                                if (isset($messageClass) && !empty($messageClass)) {
                                                    ?>
                                            <div class="<?php echo $messageClass; ?>" style="margin:10px;padding:5px;">
                                                        <?php echo $message; ?>
                                            </div>
                                                    <?php
                                                }
                                            }
                                            ?>

					<span class="login100-form-title p-b-20 p-t-15">
						Log in 
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter email Id">
						<input class="input100" type="text" name="emailId" placeholder="emailId">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-40">
						<a class="txt1" href="#">
							Forgot Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>


</body>
</html>