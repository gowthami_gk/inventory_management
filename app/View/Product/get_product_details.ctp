<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<section>
    <div class="row">
        <div class="col-md-12">
            <center><h2 class="title" style="color:blue">Product Details</h2></center>
            <div class="squiggly-border"></div>
            <section id="no-more-tables">
                <div class="row">
                    <div class="tab-content">
                        <?php if (!empty($message)) { ?>
                     
                            <center> <h3 style="color: green"> <?php echo $message; ?></h3></center>
                        <?php }
                        ?>
                        <div id="orderDetails" class="tab-pane fade in active">
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <form action="/Product/updateProduct?product_id=<?php echo $productData['id']; ?>" method="post">
                                        <tr>
                                            <td style="border-top:none;">
                                                Product ID
                                            </td>
                                            <td style="border-top:none;">
                                                <div class="col-md-8">
                                                    <?php echo $productData['id']; ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Product Name
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    <?php echo $productData['name'] ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Vendor
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    <?php echo $productData['vendor'] ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Product price
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    Rs. <?php echo $productData['mrp'] ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Batch number
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    <?php echo $productData['batch_number'] ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Batch date
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    <?php echo $productData['batch_date'] ?>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                Quantity
                                            </td>
                                            <td>
                                                <div class="col-md-8">
                                                    <input type="text" name="quantity" value="<?php echo $productData['quantity']; ?>">
                                                </div>
                                            </td>
                                        </tr>                                    
                                </table>
                                <input style="float:right" class="btn btn-info" type="submit" value="Submit">
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

        </div>
    </div>

</section>

