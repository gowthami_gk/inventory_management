<html>

    <section id="show_products">
        <div class="row">
            <div class="col-md-12">
                <div class="hero-unit non-index">

                    <center><h2>Products to be approved</h2></center>

                    <br/>
                </div>
            </div>
        </div>

            <?php if (!empty($message)) { ?>

                <center> <h3 style="color: green"> <?php echo $message; ?></h3></center>
            <?php }
            ?>       
            <div class="row">
            <div class="col-md-12">

                <section id="no-more-tables">

                    <div class="table-responsive">

                        <table class="table table-condensed">
                            <thead>
                                <tr>                                
                                    <th class="col-sm-1">No.</th>
                                    <th class="col-sm-1">Product Id</th>
                                    <th class="col-sm-1">Updated inventory</th>
                                    <th class="col-sm-1">User updated</th>
                                    <th class="col-sm-4">Updated On</th>
                                    <th class="col-sm-1">Status</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $number = 1;
                                if (isset($newProducts)) {
                                    foreach ($newProducts as $instanceProduct) {
                                        $product = $instanceProduct['ProductHistory'];
                                        ?>
                                        <tr>
                                            <td class="col-sm-1"><?php echo $number; ?></td>
                                            <td class="col-sm-1"><?php echo $product['product_id']; ?></td>
                                            <td class="col-sm-1"> <?php echo $product['updated_inventory']; ?></td>
                                            <td class="col-sm-4"><?php echo $product['user_id']; ?></td>
                                            <td class="col-sm-1"><?php echo $product['created_on']; ?></td> 
                                            
                                            <!--This form can be made only one an keep calling the same form multiple times-->   
                                            <td class="col-sm-1">                
                                                <form action="/Product/productStatusUpdate?product_id=<?php echo $product['product_id']; ?>" method="post">
                                                    <select name="status">
                                                      <option value="NEW">NEW</option>
                                                      <option value="APPROVE">APPROVE</option>
                                                      <option value="DISAPPROVE">DISAPPROVE</option>
                                                    </select>
                                                    <input type="hidden" name="quantity" value="<?php echo $product['updated_inventory']; ?>">
                                                    <input type="hidden" name="mapping_id" value="<?php echo $product['id']; ?>">


                                                    <br><br>
                                                    <input type="submit" value="Change">
                                                </form>
                                            </td>
                                        </tr> 
                                    <?php
                                    $number++;
                                    }
                                }
                                ?>
                                <?php ?>                            

                            </tbody>
                        </table>
                    </div>
                </section>

            </div>
        </div>
    </section>
</html>


