<html>

    <section id="show_products">
        <div class="row">
            <div class="col-md-12">
                <div class="hero-unit non-index">

                    <center><h2>All the products</h2></center>

                    <br/>

                    <?php
                    if ($user == 1) {
                        ?>   
                        <centre>       
                            <a href="/Product/getAllProductsWithApprovalList" class="btn btn-default"><b>Approve inventory</b></a>
                        </centre>
                        </span>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">

                <section id="no-more-tables">

                    <div class="table-responsive">

                        <table class="table table-condensed">
                            <thead>
                                <tr>                                
                                    <th class="col-sm-1">Id</th>
                                    <th class="col-sm-1">Product</th>
                                    <th class="col-sm-1">Vendor</th>
                                    <th class="col-sm-1">MRP</th>
                                    <th class="col-sm-4">Batch Number</th>
                                    <th class="col-sm-4">Batch Date</th>
                                    <th class="col-sm-1">Quantity</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (isset($products)) {
                                    foreach ($products as $instanceProduct) {
                                        $product = $instanceProduct['Product'];
                                        ?>
                                        <tr>
                                            <td class="col-sm-1">
                                                <a href='/details?productId=<?php echo $product['id']; ?>'>
                                                    <?php echo $product['id']; ?>
                                                </a>
                                            </td>

                                            <td class="col-sm-1"><?php echo $product['name']; ?></td>
                                            <td class="col-sm-1"><?php echo $product['vendor']; ?></td>
                                            <td class="col-sm-1"> Rs. <?php echo $product['mrp']; ?></td>
                                            <td class="col-sm-4"><?php echo $product['batch_number']; ?></td>
                                            <td class="col-sm-1"><?php echo $product['batch_date']; ?></td>
                                            <td class="col-sm-1"><?php echo $product['quantity']; ?></td>
                                        </tr> 

                                    <?php
                                    }
                                }
                                ?>
                                <?php ?>                            

                            </tbody>
                        </table>
                    </div>
                </section>

            </div>
        </div>
    </section>
</html>


