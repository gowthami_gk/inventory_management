<?php

App::uses('AppModel', 'Model');

/**
 * UserLogin Model
 *
 */
class User extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'user';
    public $components = array('Cookie');

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'userName' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'isDeleted' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /*
     * Function to Validate User Credentials and Return User Details
     */

    public function login($userEmailId, $password, $passwordEncrypted = false) {
        if (session_id() == '' || !isset($_SESSION)) {
            // session isn't started
            session_start();
        }
        $sessionId = session_id();

        if ($passwordEncrypted === false) {
            $password = md5($password);
        }
        $result = $this->find('first', array('conditions' => array('email' => $userEmailId)));

        if (!empty($result)) {
            $userDetails = $result['User'];

            // This password validation will be required only for normal login
            if (strcasecmp($userDetails['password'], $password) === 0) {
                // Create Session id 
                $sessionToken = md5($sessionId . $userEmailId);

                // Add this session id along with email id in session table
                $addData = array('token' => $sessionToken, 'user_email_id' => $userEmailId);

                // Importing and Declaring Models to be used
                App::import('model', 'UserSession');
                $userSessionModel = new UserSession();
                App::import('model', 'UserRoleMapping');
                $userRoleMappingModel = new UserRoleMapping();

                // Creating Session
                $userSessionResult = $userSessionModel->save($addData);
                $userDetails['userSession'] = $userSessionResult['UserSession'];

                // Set Cookies
                $userSessionModel->setUserSession($userDetails['email'], $sessionToken, $userDetails['first_name'], $userDetails['last_name']);

                $userRoleMappingData = $userRoleMappingModel->find('first', array('conditions' => array('user_id' => $userDetails['id'])));
                if (!empty($userRoleMappingData)) {
                    $userDetails['userRole'] = $userRoleMappingData['UserRoleMapping'];
                }
                // Return User
                return $userDetails;
            }
        }

        // Return False for Invalid Login
        return false;
    }

    public function getUserDetails($EmailId) {

        if (isset($EmailId)) {
            $userDetails = $this->find('first', array('conditions' => array('email' => $EmailId)));
            
            App::import('model', 'UserRoleMapping');
            $userRoleMappingModel = new UserRoleMapping();

            $userRoleMappingData = $userRoleMappingModel->find('first', array('conditions' => array('user_id' => $userDetails['User']['id'])));
            if (!empty($userRoleMappingData)) {
                $userDetails['userRole'] = $userRoleMappingData['UserRoleMapping'];
            }
            // Return User
            return $userDetails;
        }
    }

}
