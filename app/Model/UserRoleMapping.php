<?php

App::uses('AppModel', 'Model');

/**
 * Seller Model
 *
 */
class UserRoleMapping extends AppModel {

    /**
     * UserRoleMapping table
     *
     * @var mixed False or table name
     */
    public $useTable = 'user_role_mapping';
    
}
