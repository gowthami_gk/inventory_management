<?php

App::uses('AppModel', 'Model');

/**
 * Event Model
 *
 */
class UserSession extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'user_session';
    public $components = array('Cookie');
    /*
     * Function to validate user session
     */
    
     public function validate($conditions) {
        if ($this->hasAny($conditions)) {
            return $this->find('first', array('conditions' => $conditions));
        } else {
            return false;
        }
    }
    /*
     * Function to Get User Session
     */

    public function getUserSession() {
        $returnArray = array();
        
        if (!isset($_SESSION)) {
            session_start();
        }


        if (isset($_COOKIE['userEmailId']) && !empty($_COOKIE['userEmailId']) && isset($_COOKIE['sessionId']) && !empty($_COOKIE['sessionId'])) {
            $returnArray['userEmailId'] = $_COOKIE['userEmailId'];
            $returnArray['sessionId'] = $_COOKIE['sessionId'];
            
            if (isset($_COOKIE['userFirstName']) && !empty($_COOKIE['userFirstName']) && isset($_COOKIE['userLastName']) && !empty($_COOKIE['userLastName'])) {
                $returnArray['userFirstName'] = $_COOKIE['userFirstName'];
                $returnArray['userLastName'] = $_COOKIE['userLastName'];
            }
        } else if (isset($_SESSION['userEmailId']) && !empty($_SESSION['userEmailId']) && isset($_SESSION['sessionId']) && !empty($_SESSION['sessionId'])) {
            $userEmailId = $_SESSION['userEmailId'];
            $sessionId = $_SESSION['sessionId'];
            
            if (isset($_SESSION['userFirstName']) && !empty($_SESSION['userFirstName']) && isset($_SESSION['userLastName']) && !empty($_SESSION['userLastName'])) {
                $returnArray['userFirstName'] = $_SESSION['userFirstName'];
                $returnArray['userLastName'] = $_SESSION['userLastName'];
            }
        }
        
        return $returnArray;
    }

    /*
     * Function to Set User Session
     */

    public function setUserSession($userEmailId, $sessionId, $firstName = null, $lastName = null) {
        if (!isset($_SESSION)) {
            session_start();
        }
        setcookie('userEmailId', $userEmailId, time() + (86400 * 30), "/"); // 86400 = 1 day
        $_SESSION['userEmailId'] = $userEmailId;

        setcookie('sessionId', $sessionId, time() + (86400 * 30), "/");
        $_SESSION['sessionId'] = $sessionId;
        if (!empty($firstName)) {
            setcookie('userFirstName', $firstName, time() + (86400 * 30), "/");
            $_SESSION['userFirstName'] = $firstName;
            if (!empty($lastName)) {
                setcookie('userLastName', $lastName, time() + (86400 * 30), "/");
                $_SESSION['userLastName'] = $lastName;
            }
        }
    }

    /*
     * Function to Clear User Session
     */

    public function clearUserSession() {
        if (!isset($_SESSION)) {
            session_start();
        }

        setcookie('userEmailId', '', time() - 1);
        $_SESSION['userEmailId'] = '';

        setcookie('sessionId', '', time() - 1);
        $_SESSION['sessionId'] = '';

        setcookie('userFirstName', '', time() - 1);
        $_SESSION['userFirstName'] = '';

        setcookie('userLastName', '', time() - 1);
        $_SESSION['userLastName'] = '';

        // Unset All Cookies
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }

        // Return True for Logging out Successfully
        return true;
    }

    /*
     * Function to Check User Session
     */

    public function checkUserSession($userEmailId = '', $sessionId = '', $redirect = true) {
        if (!isset($_SESSION)) {
            session_start();
        }

        // If User Email Id & Session Id is not expilictly sent
        if (empty($userEmailId) || empty($sessionId)) {

            if (isset($_COOKIE['userEmailId']) && !empty($_COOKIE['userEmailId']) && isset($_COOKIE['sessionId']) && !empty($_COOKIE['sessionId'])) {
                $userEmailId = $_COOKIE['userEmailId'];
                $sessionId = $_COOKIE['sessionId'];

            } else if (isset($_SESSION['userEmailId']) && !empty($_SESSION['userEmailId']) && isset($_SESSION['sessionId']) && !empty($_SESSION['sessionId'])) {
                $userEmailId = $_SESSION['userEmailId'];
                $sessionId = $_SESSION['sessionId'];
            } else {
                if ($redirect) {
                    header("Location: /login?message=RELOGIN");
                    exit;
                } else {
                    return false;
                }
            }
        }


        // Validating the Session
        $sessionResult = $this->validate(array('user_email_id' => $userEmailId, 'token' => $sessionId));

        if (empty($sessionResult)) {
            if ($redirect) {
                header("Location: /account/login?message=INVALID_LOGIN");
                exit;
            } else {
                return false;
            }
        } else {
            return $sessionResult;
        }
    }
    

}
