<?php

App::uses('AppModel', 'Model');

/**
 * Seller Model
 *
 */
class Product extends AppModel {

    /**
     * Product table
     *
     * @var mixed False or table name
     */
    public $useTable = 'product';

    public function getAllTheProducts() {

        $allProducts = $this->find('all');
        return($allProducts);
    }

    public function getProduct($id) {

        if (isset($id)) {
            $productData = $this->find('first', array('conditions' => array('id' => $id)));
            return $productData;
        }
    }

}
