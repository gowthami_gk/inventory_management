<?php

App::uses('AppModel', 'Model');

/**
 * Seller Model
 *
 */
class UserRole extends AppModel {

    /**
     * UserRole table
     *
     * @var mixed False or table name
     */
    public $useTable = 'user_role';
    
}
