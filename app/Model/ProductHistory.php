<?php

App::uses('AppModel', 'Model');

/**
 * Seller Model
 *
 */
class ProductHistory extends AppModel {

    /**
     * ProductHistory table
     *
     * @var mixed False or table name
     */
    public $useTable = 'product_history';
    
    public function newProducts(){
        
        $newProducts = $this->find('all', array('conditions' => array('status' => 'NEW')));
        return $newProducts;
    }
}
