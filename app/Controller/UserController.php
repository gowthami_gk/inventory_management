<?php

App::uses('AppController', 'Controller', 'AppModel');

class UserController extends AppController {

    public $uses = array('User', 'UserSession');

    public function index() {

    }    
    public function submit() {

        $this->autoRender = false;
        $sessionId = 0;
        if ($this->request) {

            // Post Data
            $postData = $_REQUEST;

            if (!empty($postData)) {

                if (isset($postData['emailId']) && !empty($postData['emailId']) &&
                        isset($postData['password']) && !empty($postData['password'])) {
                    $data = $this->User->login($postData['emailId'], $postData['password']);
                    if ($data) {
                        
                        return $this->redirect(array('controller' => 'Product', 'action' => 'getAllProducts'));
                        
                        $data['messageType'] = 'SUCCESS';
                        $data['message'] = 'Login Successful';
                    } else {
                        $data['messageType'] = 'FAILURE';
                        $data['message'] = 'EmailId / password is incorrect';
                    }
                } else {
                    $data['messageType'] = 'FAILURE';
                    $data['message'] = 'PARAMETERS NOT SET';
                }
            } else {
                $data['messageType'] = 'FAILURE';
                $data['message'] = 'EMPTY POST DATA';
            }
        } else {
            $data['messageType'] = 'FAILURE';
            $data['message'] = 'NOT A POST';
        }
        
        echo json_encode($data);
        exit;
    }
    
    public function logout(){
        $userSessionDetail = $this->UserSession->clearUserSession();
        return $this->redirect(array('controller' => 'user', 'action' => 'index'));
    }

}
