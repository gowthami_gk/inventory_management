<?php

App::uses('AppController', 'Controller', 'AppModel');

class ProductController extends AppController {

    public $uses = array('UserSession', 'Product', 'ProductHistory', 'User');

    public function index() {
        
    }

    public function getAllProductsWithApprovalList() {

        $userSessionDetail = $this->UserSession->getUserSession();
        $userSessionObj = $this->UserSession->checkUserSession($userSessionDetail['userEmailId'], $userSessionDetail['sessionId'], false);
        if ($userSessionObj) {

            $userData = $this->User->getUserDetails($userSessionObj['UserSession']['user_email_id']);
            if ($userData['userRole']['user_role_id'] == 2) {
                $newProducts = $this->ProductHistory->newProducts();
                $this->set('newProducts', $newProducts);
                if (!empty($_REQUEST['message'])) {
                    $this->set('message', $_REQUEST['message']);
                }
            } else {

                $message = "You do not have access to this page, please contact your store manager";
                $messageType = "FAILURE";

                $queryParameters = array('message' => $message, 'messageType' => $messageType);
                $json = json_encode($queryParameters);
                echo $json;
                exit;
            }
        } else {
            $message = "Login session expired. Please login";
            $messageType = "SESSION_EXPIRED";

            $queryParameters = array('message' => $message, 'messageType' => $messageType);
            $json = json_encode($queryParameters);
            echo $json;
            exit;
        }
    }

    public function getAllProducts() {

        $userSessionDetail = $this->UserSession->getUserSession();

        $userSessionObj = $this->UserSession->checkUserSession($userSessionDetail['userEmailId'], $userSessionDetail['sessionId'], false);
        if ($userSessionObj) {

            $userData = $this->User->getUserDetails($userSessionObj['UserSession']['user_email_id']);
            $storteManager = 0;
            if ($userData['userRole']['user_role_id'] == 2) {
                $storteManager = 1;
            }
            $products = $this->Product->getAllTheProducts();
            if (!empty($products)) {
                $this->set('products', $products);
                $this->set('user', $storteManager);
            } else {
                $message = "Products does not exist";
                $messageType = "FAILURE";
            }
        } else {
            $message = "Login session expired. Please login";
            $messageType = "SESSION_EXPIRED";
            $queryParameters = array('message' => $message, 'messageType' => $messageType);
            $json = json_encode($queryParameters);
            echo $json;
            exit;
        }
    }

    public function getProductDetails() {

        $userSessionDetail = $this->UserSession->getUserSession();
        $userSessionObj = $this->UserSession->checkUserSession($userSessionDetail['userEmailId'], $userSessionDetail['sessionId'], false);
        if ($userSessionObj) {

            //Currently, we have only one field to edit, this may not be required, but in the future if you need to edit multiple field they would be useful
            if (!empty($_REQUEST)) {

                $productId = $_REQUEST['productId'];

                $productDetails = $this->Product->getProduct($productId);

                if (!empty($productDetails)) {
                    $this->set('productData', $productDetails['Product']);
                    if (!empty($_REQUEST['message'])) {
                        $this->set('message', $_REQUEST['message']);
                    }
                }
            }
        } else {
            $message = "Login session expired. Please login";
            $messageType = "SESSION_EXPIRED";

            $queryParameters = array('message' => $message, 'messageType' => $messageType);
            $json = json_encode($queryParameters);
            echo $json;
            exit;
        }
    }

    public function updateProduct() {

        if (!empty($_REQUEST)) {
            $userSessionDetail = $this->UserSession->getUserSession();
            $userSessionObj = $this->UserSession->checkUserSession($userSessionDetail['userEmailId'], $userSessionDetail['sessionId'], false);
            if ($userSessionObj) {
                $productId = $_REQUEST['product_id'];
                $quantity = $_REQUEST['quantity'];
                $userData = $this->User->getUserDetails($userSessionObj['UserSession']['user_email_id']);

                $utc = gmdate("M d Y h:i:s A");
                $date = new DateTime($utc);
                $time = new DateTimeZone('Asia/Kolkata');
                $date->setTimezone($time);

                $now = $date->format('Y-m-d H:i:s');

                if ($userData['userRole']['user_role_id'] == 2) {

                    //Previous value canalso be saved fo rthe future reference

                    $updatedQuantity = $this->Product->updateAll(
                            array('quantity' => "'" . $quantity . "'"), array('id' => $productId)  //condition
                    );
                    $productHistory = $this->ProductHistory->save(array('product_id' => $productId, 'updated_inventory' => $quantity, 'user_id' => $userData['User']['id'], 'status' => 'APPROVED', 'created_on' => $now));

                    $message = "Prduct is updated successful";
                    $messageType = "SUCCESS";
                    //update history
                } else if ($userData['userRole']['user_role_id'] == 1) {
                    $productHistory = $this->ProductHistory->save(array('product_id' => $productId, 'updated_inventory' => $quantity, 'user_id' => $userData['User']['id'], 'status' => 'NEW', 'created_on' => $now));
                    $message = "Prduct is updated, waiting for an approval";
                    $messageType = "SUCCESS";
                }
                return $this->redirect(array('controller' => 'Product', 'action' => 'getProductDetails?productId=' . $productId . '&message=' . $message));
            } else {
                $message = "Login session expired. Please login";
                $messageType = "SESSION_EXPIRED";

                $queryParameters = array('message' => $message, 'messageType' => $messageType);
                $json = json_encode($queryParameters);
                echo $json;
                exit;
            }
        }
    }

    public function productStatusUpdate() {

        if (isset($_REQUEST)) {
            $userSessionDetail = $this->UserSession->getUserSession();
            $userSessionObj = $this->UserSession->checkUserSession($userSessionDetail['userEmailId'], $userSessionDetail['sessionId'], false);
            if ($userSessionObj) {

                $userData = $this->User->getUserDetails($userSessionObj['UserSession']['user_email_id']);
                if ($userData['userRole']['user_role_id'] == 2) {

                    $productId = $_REQUEST['product_id'];
                    $status = $_REQUEST['status'];
                    $mappingId = $_REQUEST['mapping_id'];

                    if ($status == 'APPROVE') {
                        $newQuantity = $_REQUEST['quantity'];
                        $updatedQuantity = $this->Product->updateAll(
                                array('quantity' => "'" . $newQuantity . "'"), array('id' => $productId)  //condition
                        );
                        $updatedQuantity = $this->ProductHistory->updateAll(
                                array('status' => "'APPROVED'"), array('id' => $mappingId)  //condition
                        );

                        $message = "Inventory request approved";
                    } else if ($status == 'DISAPPROVE') {
                        $updatedQuantity = $this->ProductHistory->updateAll(
                                array('status' => "'DISAPPROVED'"), array('id' => $mappingId)  //condition
                        );
                        $message = "Inventory request disapproved";
                    }

                    return $this->redirect(array('controller' => 'Product', 'action' => 'getAllProductsWithApprovalList?message=' . $message));
                } else {

                    $message = "You do not have access to this page, please contact your store manager";
                    $messageType = "FAILURE";
                    $queryParameters = array('message' => $message, 'messageType' => $messageType);
                    $json = json_encode($queryParameters);
                    echo $json;
                    exit;
                }
            } else {
                $message = "Login session expired. Please login";
                $messageType = "SESSION_EXPIRED";

                $queryParameters = array('message' => $message, 'messageType' => $messageType);
                $json = json_encode($queryParameters);
                echo $json;
                exit;
            }
        }
    }

}
